'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var app = function () {
    function app() {
        var _this = this;

        _classCallCheck(this, app);

        document.querySelectorAll('.accordion__input').forEach(function (input) {
            return input.addEventListener('click', _this.accordion);
        });
        document.querySelectorAll('.icon--remove').forEach(function (ico) {
            return ico.addEventListener('click', _this.remove);
        });
        document.querySelectorAll('.input--add').forEach(function (input) {
            return input.addEventListener('click', _this.add);
        });
    }

    _createClass(app, [{
        key: 'accordion',
        value: function accordion() {
            document.querySelectorAll('.accordion__item').forEach(function (input) {
                return input.classList.remove('accordion__item--selected');
            });
            this.closest('.accordion__item').classList.add('accordion__item--selected');
        }
    }, {
        key: 'remove',
        value: function remove() {
            this.closest('.domains__fields').remove();
        }
    }, {
        key: 'add',
        value: function add() {
            var html = '      <div class="domains__fields">\n' + '                    <div class="domains__field">\n' + '                        <input type="text" class="input input--text">\n' + '                    </div>\n' + '                    <div class="domains__field">\n' + '                        <input type="text" class="input input--text">\n' + '                    </div>\n' + '                    <div class="domains__field">\n' + '                        <select name="" id="" class="input input--select">\n' + '                            <option value="">NO</option>\n' + '                            <option value="">SSL</option>\n' + '                        </select>\n' + '                    </div>\n' + '                    <div class="domains__field domains__field--last">\n' + '                        <img src="img/delete_icon.svg" class=\'icon icon--remove\' alt="icon">\n' + '                    </div>\n' + '                </div>';
            this.closest('.domains__fields').insertAdjacentHTML('beforebegin', html);
        }
    }]);

    return app;
}();

var application = new app();