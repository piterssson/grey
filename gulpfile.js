'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat');
var babel = require('gulp-babel');

gulp.task('sass', function () {
    return gulp.src('scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('compile', function () {
    return gulp.src(['./css/bootstrap/bootstrap-grid.css', './css/mine/mine.css'])
        .pipe(cssmin())
        .pipe(concat('./css/style.css'))
        .pipe(gulp.dest(''));
});

gulp.task('watch', function () {
    gulp.watch('scss/**/*.scss', ['sass']);
    gulp.watch(['./css/bootstrap/bootstrap-grid.css', './css/mine/mine.css'], ['compile']);
});

gulp.task('js', () =>
    gulp.src('js/app.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist'))
);