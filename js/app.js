class app {

    constructor() {
        document.querySelectorAll('.accordion__input').forEach(input => input.addEventListener('click', this.accordion));
        document.querySelectorAll('.icon--remove').forEach(ico => ico.addEventListener('click', this.remove));
        document.querySelectorAll('.input--add').forEach(input => input.addEventListener('click', this.add));
    }

    accordion() {
        document.querySelectorAll('.accordion__item').forEach(input => input.classList.remove('accordion__item--selected'));
        this.closest('.accordion__item').classList.add('accordion__item--selected');
    }

    remove() {
        this.closest('.domains__fields').remove();
    }

    add() {
        const html = '      <div class="domains__fields">\n' +
            '                    <div class="domains__field">\n' +
            '                        <input type="text" class="input input--text">\n' +
            '                    </div>\n' +
            '                    <div class="domains__field">\n' +
            '                        <input type="text" class="input input--text">\n' +
            '                    </div>\n' +
            '                    <div class="domains__field">\n' +
            '                        <select name="" id="" class="input input--select">\n' +
            '                            <option value="">NO</option>\n' +
            '                            <option value="">SSL</option>\n' +
            '                        </select>\n' +
            '                    </div>\n' +
            '                    <div class="domains__field domains__field--last">\n' +
            '                        <img src="img/delete_icon.svg" class=\'icon icon--remove\' alt="icon">\n' +
            '                    </div>\n' +
            '                </div>';
        this.closest('.domains__fields').insertAdjacentHTML('beforebegin',html);
    }

}

let application = new app();